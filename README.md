# WebPA - An Online Peer Assessment System for HE - Forked with changes

Original Copyright (c) 2004-2013, Loughborough University http://webpaproject.lboro.ac.uk/


## What is it?

WebPA is an online peer assessment system, or more specifically, a peermoderated
marking system. It is designed for teams of students doing groupwork,
the outcome of which earns an overall group mark. Each student in a
group grades their team-mates (and their own) performance. This grading is
then used with the overall group mark to provide each student with an
individual grade. The individual grade reflects the students contribution to the
group.

## Getting Started

First, you'll need to install and configure WebPA.
Details of the installation process can be found in the installation guide located in the documentation folder.

## Changes as of this version

This version contains a series of changes aiming to improve WebPA for all users:


* Updated to PHP 7 standards, including use of PDO
* MD5 password hashing replaced with password_hash and password_verify functions
* Improved user interface design including support for mobile devices
* Simplified installation process with new installer
* Additional tutor option: recurring assessments
* Admin control panel added to change WebPA internal settings (these are now stored in the database)
* New documentation added

## Licensing

Please see the file called licence.txt.
