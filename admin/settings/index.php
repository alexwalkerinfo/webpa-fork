<?php
/**
 *
 * landing page for the settings section of the admin area
 *
 * From here admins can alter settings of WebPA
 *
 * @copyright 2008 Loughborough University
 * @license http://www.gnu.org/licenses/gpl.txt
 * @version 0.0.0.1
 * @since 22 Feb 2018
 *
 */

//get the include file required
require_once("../../includes/inc_global.php");
require_once(DOC__ROOT . 'includes/functions/lib_university_functions.php');

// If there's setting to update
if (!empty($_POST)) {
  $error = "";

  foreach ($_POST as $key => $val) {
    $_fields = array(
      ':setting_value'  => substr($val, 0, 255),
      ':setting_lastchangedby' => $_user->id,
      ':key' => substr($key, 0, 255)
    );

    // Get existing setting value
    $existing_value = $DB->fetch_value("SELECT setting_value FROM " . APP__DB_TABLE_PREFIX . "setting WHERE setting_internalid = ?", array($key));

    // Check setting actually needs to be changed
    if ($existing_value != $val) {
      // Error checking
      switch ($key) {
        // URLs
        case "APP__HELP_LINK":
        case "APP__INST_LOGO":
        case "APP__WWW":
          if (!filter_var($val, FILTER_VALIDATE_URL)) {
            $error .= '<p class="error">The URL entered was invalid. Please try again.</p>';
          }
          break;

        // Emails
        case "APP__EMAIL_NO_REPLY":
        case "APP__EMAIL_HELP":
        if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
          $error .= '<p class="error">The email entered was invalid. Please try again.</p>';
        }
        break;
      }

      // Save if no errors found
      if ($error == "") {
        $DB->execute("UPDATE " . APP__DB_TABLE_PREFIX . "setting SET setting_value = :setting_value, setting_lastchangedby = :setting_lastchangedby
        WHERE setting_internalid = :key", $_fields);
      }
    }
   }

   if ($error == "") {
   // Ensures changes of settings are in effect since we load them before we change them
   header('Refresh: 0');
 }
}

if (!check_user($_user, APP__USER_TYPE_ADMIN)) {
  header('Location:'. APP__WWW .'/logout.php?msg=denied');
  exit;
}

$intro_text = "<p>This page allows you to alter some of the main WebPA settings.</p>";
$task_text = "<p>If changes are required ensure to click save once completed.</p>";

//set the page information
$UI->page_title = APP__NAME . " settings ";
$UI->menu_selected = 'settings';
$UI->breadcrumbs = array ('home' => '../../');
$UI->help_link = '?q=node/237';
$UI->head();
?>
<style type="text/css">

</style>
<?php

$UI->body();
$UI->content_start();

echo $intro_text;
echo "<div class=\"content_box\">";

if (isset($error)) {
  echo $error;
}

echo $task_text;

$settings = $DB->fetch("SELECT * FROM " . APP__DB_TABLE_PREFIX . "setting");
?>

<form action="index.php" method="post">
      <?php
      foreach ($settings as $setting) {
        echo('<div class="row">');
        echo('<div class="col1"><label for="' . $setting['setting_internalid'] . '">' . $setting['setting_name'] . '</label></div>');

        // Start year is a selection
        if ($setting['setting_internalid'] == "APP__ACADEMIC_YEAR_START_MONTH" ) {
            echo('<div class="col3"><select name="' . $setting['setting_internalid'] . '">');

            // Populate options with each month
            for ($i = 1; $i <= 12; ++$i) {
              if ($i == $setting['setting_value']) {
                echo('<option selected="selected" value="' . $i .'">' . $i . '</option>');
              } else {
                echo('<option value="' . $i .'">' . $i . '</option>');
              }
            }
            echo('</select></div>');

        // Timezone is also a selection
        } else if ($setting['setting_internalid'] == "APP__TIMEZONE") {

            echo('<div class="col3"><select name="' . $setting['setting_internalid'] . '">');

            $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

            foreach ($timezones as $key => $value) {
              if ($value == $setting['setting_value']) {
                echo('<option selected="selected" value="' . $value . '">' . $value . '</option>');
              } else {
                echo('<option value="' . $value . '">' . $value . '</option>');
              }
            }

            echo('</select></div>');

        // Otherwise a text entry
        } else {
            echo('<div class="col3"><input type="text" value="' . $setting['setting_value'] . '" name="'. $setting['setting_internalid'] . '"></div>');
        }

        echo('<div class="col3">' . $setting['setting_description']);

        // Logo doesn't have a default value so only display if necessary
        if ($setting['setting_defaultvalue'] != "") {
            echo(' e.g. ' . $setting['setting_defaultvalue'] . '</div>');
        } else {
          echo('</div>');
        }

        echo('</div>');
      }
      ?>
  <input type="submit" value="Save Changes">
</form>
</div>
<?php

$UI->content_end();

?>
