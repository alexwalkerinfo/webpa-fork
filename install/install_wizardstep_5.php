<!DOCTYPE html>
<html>
<head>
  <title>Install WebPA - Finish</title>
  <link rel="stylesheet" type="text/css" href="css/install.css">
</head>

<body>
<div id="header">
  <div id="app_bar">
    <div id="title_logo">
      <img style="vertical-align: middle;" src="../images/tool/appbar_webpa_logo.png" alt="WebPA"/>
    </div>
    <div id="title_text">Install your WebPA instance...</div>
  </div>
</div>

<div id="container">
  <div id="main">
    <div id="content">
      <div id="stagebar">
        <div class="text">Installation - (1) Begin > (2) Database Information > (3) Administrator Setup > (4) Institution Information > <b>(5) Finish</b></div>
      </div>
      <div class="content_box">
        <div class="text">
          <h1>Finish</h1>
          <p>The WebPA installation has been completed!</p>
          <p><a href="../">Click to go to your new WebPA Installation!</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
