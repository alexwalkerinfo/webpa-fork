<?php
ob_start();

if (isset($_POST["admin_uname"]) && isset($_POST["admin_pass"]) && isset($_POST["admin_fname"])
  && isset($_POST["admin_lname"]) && isset($_POST["admin_email"])) {

  $error = "";

  if (!filter_var($_POST["admin_email"], FILTER_VALIDATE_EMAIL)) {
    $error .= '<p class="error">Bad email format. Please try again.</p>';
  } else {
    // Necessary information posted?
    require_once("../includes/classes/class_installer.php");
    require_once("../includes/inc_db_settings.php");

    // New installer instance
    $install = new Installer(APP__DB_DRIVER, APP__DB_HOST, APP__DB_DATABASE, APP__DB_USERNAME, APP__DB_PASSWORD, APP__DB_TABLE_PREFIX);

    // Check connection
    if ($install->open()) {
      $sql = "INSERT INTO " . APP__DB_TABLE_PREFIX . "user (user_id, forename, lastname, email, username, password, admin, disabled)
      VALUES (1, ?, ?, ?, ?, ?, 1, 0)";

      if ($install->prepared_stmt($sql, array($_POST["admin_fname"], $_POST["admin_lname"],
      $_POST["admin_email"], $_POST["admin_uname"], password_hash($_POST["admin_pass"], PASSWORD_DEFAULT)))) {
        header('Location: install_wizardstep_4.php');
      } else {
        $error .= '<p class="error">Failed to add admin account. Admin may already exist.</p>';
      }
    } else {
      $error .= '<p class="error">Couldn\'t create connection to database. Please check the connection settings.</p>';
    }
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Install WebPA - Administrator Setup</title>
  <link rel="stylesheet" type="text/css" href="css/install.css">
</head>

<body>
<div id="header">
  <div id="app_bar">
    <div id="title_logo">
      <img style="vertical-align: middle;" src="../images/tool/appbar_webpa_logo.png" alt="WebPA"/>
    </div>
    <div id="title_text">Install your WebPA instance...</div>
  </div>
</div>

<div id="container">
  <div id="main">
    <div id="content">
      <div id="stagebar">
        <div class="text">Installation - (1) Begin > (2) Database Information > <b>(3) Administrator Setup</b> > (4) Institution Information > (5) Finish</div>
      </div>
      <div class="content_box">
        <div class="text">
          <h1>Administrator Setup</h1>
          <?php
          if (isset($error)) {
            echo $error;
          }
          ?>
          <form action="install_wizardstep_3.php" method="post">
            <fieldset>
              <legend>Administrator Setup</legend>
              <p>Enter details for the first WebPA admin account.</p>
              <table>
                <tr>
                  <td><label for="admin_uname">Username:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['admin_uname'])) { echo $_POST['admin_uname'] . "\""; } else { ?>admin" <?php }?> name="admin_uname" required></td>
                </tr>
                <tr>
                  <td><label for="admin_pass">Password:</label></td>
                  <td><input type="password" name="admin_pass" required></td>
                </tr>
                <tr>
                  <td><label for="admin_fname">Forename:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['admin_fname'])) { echo $_POST['admin_fname']; }?>" name="admin_fname" required></td>
                </tr>
                <tr>
                  <td><label for="admin_lname">Last Name:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['admin_lname'])) { echo $_POST['admin_lname']; }?>" name="admin_lname" required></td>
                </tr>
                <tr>
                  <td><label for="admin_email">Email:</label></td>
                  <td><input type="email" value="<?php if (isset($_POST['admin_email'])) { echo $_POST['admin_email']; }?>" name="admin_email" required></td>
                </tr>
              </table>
              <p>You can alter the details of this account later at any time in the WebPA admin control panel.</p>
            </fieldset>
            <input type="submit" value="Proceed">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
