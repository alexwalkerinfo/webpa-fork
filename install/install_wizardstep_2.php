<?php
// Necessary information posted?
if (isset($_POST["db_driver"]) && isset($_POST["db_host"]) && isset($_POST["db_name"]) &&
  isset($_POST["db_user"]) && isset($_POST["db_pass"]) && isset($_POST["db_prefix"])) {

  require_once("../includes/classes/class_installer.php");

  // New installer instance
  $install = new Installer($_POST["db_driver"], $_POST["db_host"], $_POST["db_name"], $_POST["db_user"], $_POST["db_pass"], $_POST["db_prefix"]);
  $error = "";

  // Check connection
  if ($install->open()) {
    // Write settings to file
    if ($install->write_settings()) {
      // Create tables
      if ($install->create_tables()) {
        // Success! Move to next stage
        header("Location: install_wizardstep_3.php");
      } else {
        $error .= '<p class="error">Couldn\'t create database tables. Please check the database is empty and try again.</p>';
      }
    } else {
      $error .= '<p class="error">Couldn\'t write the settings file. Please check the install folder has permissions to write.</p>';
    }
  } else {
    $error .= '<p class="error">Couldn\'t create connection to database. Please check the connection settings.</p>';
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Install WebPA - Database Information</title>
  <link rel="stylesheet" type="text/css" href="css/install.css">
</head>

<body>
<div id="header">
  <div id="app_bar">
    <div id="title_logo">
      <img style="vertical-align: middle;" src="../images/tool/appbar_webpa_logo.png" alt="WebPA"/>
    </div>
    <div id="title_text">Install your WebPA instance...</div>
  </div>
</div>

<div id="container">
  <div id="main">
    <div id="content">
      <div id="stagebar">
        <div class="text">Installation - (1) Begin > <b>(2) Database Information</b> > (3) Administrator Setup > (4) Institution Information > (5) Finish</div>
      </div>
      <div class="content_box">
        <div class="text">
          <h1>Database Information</h1>
          <?php
          if (isset($error)) {
            echo $error;
          }
          ?>
          <form action="install_wizardstep_2.php" method="post">
            <fieldset>
              <legend>Database Information</legend>
              <p>Enter the details of the database WebPA can use for installation.</p>
              <table>
                <tr>
                  <td><label for="db_driver">Database Server:</label></td>
                  <td>
                    <select name="db_driver">
                      <option value="mysql">MariaDB / MySQL</option>
                      <!--<option value="sqlsrv">Microsoft SQL</option>-->
                    </select>
                  </td>
                </tr>
                <tr>
                  <td><label for="db_host">Database Hostname:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['db_host'])) { echo $_POST['db_host'] . "\""; } else { ?>localhost" <?php }?> name="db_host" required></td>
                </tr>
                <tr>
                  <td><label for="db_name">Database Name:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['db_name'])) { echo $_POST['db_name'] . "\""; } else { ?>webpa" <?php }?> name="db_name" required></td>
                </tr>
                <tr>
                  <td><label for="db_user">Database User:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['db_user'])) { echo $_POST['db_user'] . "\""; } else { ?>username" <?php }?> name="db_user" required></td>
                </tr>
                <tr>
                  <td><label for="db_pass">Database Password:</label></td>
                  <td><input type="password" name="db_pass" required></td>
                </tr>
                <tr>
                  <td><label for ="db_prefix">Table Prefix:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['db_prefix'])) { echo $_POST['db_prefix'] . "\""; } else { ?>pa2_" <?php }?> name="db_prefix" required></td>
                </tr>
              </table>
              <p>The database user must have full permissions to the database.</p>
            </fieldset>
            <input type="submit" value="Proceed">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
