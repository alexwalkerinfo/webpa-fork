<!DOCTYPE html>
<html>
<head>
  <title>Install WebPA - Begin Installation</title>
  <link rel="stylesheet" type="text/css" href="css/install.css">
</head>

<body>
<div id="header">
  <div id="app_bar">
    <div id="title_logo">
      <img style="vertical-align: middle;"
      src="../images/tool/appbar_webpa_logo.png" alt="WebPA"/>
    </div>
    <div id="title_text">Install your WebPA instance...</div>
  </div>
</div>

<div id="container">
  <div id="main">
    <div id="content">
      <div id="stagebar">
        <div class="text">Installation - <b>(1) Begin</b> > (2) Database Information > (3) Institution Information > (4) Administrator Setup > (5) Finish</div>
      </div>
      <div class="content_box">
        <div class="text">
          <h1>Begin WebPA Installation</h1>
          <p>To begin the installation of WebPA you will require the following:</p>
          <ol>
            <li>A database server running MariaDB, MySQL, or MSSQL.</li>
            <li>An empty database with no previous WebPA installation.</li>
            <li>A database server username and password with permissions to access / modify / delete from the empty database.</li>
            <li>Table prefix (for running additional installations if necessary).</li>
          </ol>
          <button type="button" onclick="location.href='install_wizardstep_2.php'">Begin</button>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
