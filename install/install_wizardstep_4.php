<?php
if (isset($_POST["inst_name"]) && isset($_POST["inst_url"]) && isset($_POST["inst_email"]) &&
  isset($_POST["inst_noreplyemail"]) && isset($_POST["inst_startmonth"])) {

  $error = "";

  if (!filter_var($_POST["inst_email"], FILTER_VALIDATE_EMAIL) && !filter_var($_POST["inst_noreplyemail"], FILTER_VALIDATE_EMAIL)) {
    $error .= '<p class="error">Bad email format. Please try again.</p>';
  } else {
    // Necessary information posted?
    require_once("../includes/classes/class_installer.php");
    require_once("../includes/inc_db_settings.php");

    // New installer instance
    $install = new Installer(APP__DB_DRIVER, APP__DB_HOST, APP__DB_DATABASE, APP__DB_USERNAME, APP__DB_PASSWORD, APP__DB_TABLE_PREFIX);

    // Check connection
    if ($install->open()) {
      $sql = "INSERT INTO " . APP__DB_TABLE_PREFIX . "setting (setting_name, setting_internalid, setting_value, setting_description, setting_defaultvalue, setting_lastchangedby) VALUES
      ('Institution Name', 'APP__NAME', ?, 'This is the name of your institution.', 'My Institution', '1'),
      ('Institution URL', 'APP__WWW', ?, 'The URL to your WebPA installation.', 'http://www.webpa.com', '1'),
      ('Contact Email', 'APP__EMAIL_HELP', ?, 'The email used for the contact page.', 'someone@email.com', '1'),
      ('No-reply Email', 'APP__EMAIL_NO_REPLY', ?, 'An email for outgoing messages only.', 'no-reply@email.com', '1'),
      ('Help Link', 'APP__HELP_LINK', 'http://webpaproject.com', 'The URL that the \'Help\' button is linked to', 'http://webpaproject.com', '1'),
      ('Logo URL', 'APP__INST_LOGO', ?, 'Image should preferably be 50px in height.', '', '1'),
      ('Logo Alt Text', 'APP__INST_LOGO_ALT', 'Your institution name', 'The alternative text for the logo.', 'Your institution name', '1'),
      ('Academic Start Month', 'APP__ACADEMIC_YEAR_START_MONTH', ?, 'The starting month of the academic year. Always 1st of month.', '9 (September)', '1'),
      ('Timezone', 'APP__TIMEZONE', ?, 'Your timezone.', 'Europe/London', '1')";

      // Remove any trailing /
      $trimmedUrl = rtrim($_POST["inst_url"], '/');

      if ($install->prepared_stmt($sql, array(
        $_POST["inst_name"],
        $trimmedUrl,
        $_POST["inst_email"],
        $_POST["inst_noreplyemail"],
        $trimmedUrl . "/images/logo.png",
        $_POST["inst_startmonth"],
        $_POST["inst_timezone"]
        ))) {

        // Insert sample module
        $sql = "INSERT INTO " . APP__DB_TABLE_PREFIX . "module SET
        module_code = ?, module_title = ?;";

        if ($install->prepared_stmt($sql, array("sample", "Sample module"))) {
          // Finished
          header('Location: install_wizardstep_5.php');
        } else {
          $error .= '<p class="error">Was unable to add sample module to database. Perhaps it already exists?</p>';
        }
      } else {
        $error .= '<p class="error">Unable to add institution information to database. Perhaps it already exists?</p>';
      }
    } else {
      $error .= '<p class="error">Couldn\'t create connection to database. Please check the connection settings.</p>';
    }
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Install WebPA - Institution Information</title>
  <link rel="stylesheet" type="text/css" href="css/install.css">
</head>

<body>
<div id="header">
  <div id="app_bar">
    <div id="title_logo">
      <img style="vertical-align: middle;" src="../images/tool/appbar_webpa_logo.png" alt="WebPA"/>
    </div>
    <div id="title_text">Install your WebPA instance...</div>
  </div>
</div>

<div id="container">
  <div id="main">
    <div id="content">
      <div id="stagebar">
        <div class="text">Installation - (1) Begin > (2) Database Information > (3) Administrator Setup > <b>(4) Institution Information</b> > (5) Finish</div>
      </div>
      <div class="content_box">
        <div class="text">
          <h1>Institution Information</h1>
          <?php
          if (isset($error)) {
            echo $error;
          }
          ?>
          <form action="install_wizardstep_4.php" method="post">
            <fieldset>
              <legend>Institution Information</legend>
              <p>Enter your institution information for WebPA's installation.</p>
              <table>
                <tr>
                  <td><label for="inst_name">Institution Name:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['inst_name'])) { echo $_POST['inst_name']; }?>" name="inst_name" required></td>
                </tr>
                <tr>
                  <td><label for="inst_url">WebPA URL:</label></td>
                  <td><input type="text" value="<?php if (isset($_POST['inst_url'])) { echo $_POST['inst_url']; }?>" name="inst_url" required></td>
                </tr>
                <tr>
                  <td><label for="inst_email">Contact Email:</label></td>
                  <td><input type="email" value="<?php if (isset($_POST['inst_email'])) { echo $_POST['inst_email']; }?>" name="inst_email" required></td>
                </tr>
                <tr>
                  <td><label for="inst_noreplyemail">No-reply Email:</label></td>
                  <td><input type="email" value="<?php if (isset($_POST['inst_noreplyemail'])) { echo $_POST['inst_noreplyemail']; }?>" name="inst_noreplyemail" required></td>
                </tr>
                <tr>
                  <td><label for="inst_startmonth">Academic Starting Month:</label></td>
                  <td>
                    <select name="inst_startmonth">
                      <?php
                      // This code adds each month number to the selection box
                      if (isset($_POST["inst_startmonth"])) {
                        $startmonth = $_POST["inst_startmonth"];
                      } else {
                        $startmonth = 9;
                      }
                      for ($i = 1; $i <= 12; ++$i) {
                        if ($i == $startmonth) {
                          echo('<option selected="selected" value="' . $i .'">' . $i . '</option>');
                        } else {
                          echo('<option value="' . $i .'">' . $i . '</option>');
                        }
                      }
                      ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td><label for="inst_timezone">Timezone:</label></td>
                  <td>
                    <select name="inst_timezone">
                    <?php
                    // Add each timezone
                    $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
                    if (isset($_POST["inst_timezone"])) {
                      $default_timezone = $_POST["inst_timezone"];
                    } else {
                      $default_timezone = "Europe/London";
                    }
                    foreach ($timezones as $key => $value) {
                      if ($value == $default_timezone) {
                        echo('<option selected="selected" value="' . $value . '">' . $value . '</option>');
                      } else {
                        echo('<option value="' . $value . '">' . $value . '</option>');
                      }
                    }
                    ?>
                    </select>
                  </td>
                </tr>
              </table>
              <p>This information is editable later in the WebPA admin control panel.</p>
            </fieldset>
            <input type="submit" value="Proceed">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
