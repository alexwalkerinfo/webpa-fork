<?php
/**
 *
 * Class : DBAuthenticator
 *
 * Authenticates the given username and password against the internal database
 *
 *
 * @copyright 2007 Loughborough University
 * @license http://www.gnu.org/licenses/gpl.txt
 * @version 1.0.0.1
 *
 */

class DBAuthenticator extends Authenticator {

/*
================================================================================
  PUBLIC
================================================================================
*/

  /*
  Authenticate the user against the internal database
  */
  function authenticate() {

    $this->_error = NULL;

    // Find the user's password by their username. In order to use password_verify, we can't select the row by password as previously.
    $sql = 'SELECT password FROM ' . APP__DB_TABLE_PREFIX . "user WHERE (username = ?) AND (source_id = '')";
    $DAO = $this->get_DAO();
    $password = $DAO->fetch_value($sql, array($this->username));

    // Select user row SQL
    $sql = 'SELECT * FROM ' . APP__DB_TABLE_PREFIX . "user WHERE (username = ?) AND (source_id = '')";
    $vars = array($this->username);

    // Check password is correct
    if (password_verify($this->password, $password)) {
      return $this->initialise($sql, $vars);
    } else {
      return false;
    }

  }// /->authenticate()

/*
================================================================================
  PRIVATE
================================================================================
*/

}// /class DBAuthenticator

?>
