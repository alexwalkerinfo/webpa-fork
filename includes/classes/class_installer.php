<?php
/**
 *
 * Class : Installer
 *
 * Installer object for installing WebPA automatically
 *
 *
 *
 * @copyright 2018 Alexander Walker
 * @license http://www.gnu.org/licenses/gpl.txt
 * @version 1.5.0.0
 *
 *
 */

require("class_dao.php");

class Installer {

  // Database settings
  private $db_driver;
  private $db_host;
  private $db_name;
  private $db_user;
  private $db_pass;
  private $db_prefix;
  private $db;

  // Construct installer
  function __construct($dbdriver, $dbhost, $dbname, $dbuser, $dbpass, $dbprefix) {
    $this->db_driver = $dbdriver;
    $this->db_host = $dbhost;
    $this->db_name = $dbname;
    $this->db_user = $dbuser;
    $this->db_pass = $dbpass;
    $this->db_prefix = $dbprefix;
  }

  // Connect to Database
  function open() {
      $this->db = new DAO($this->db_driver, $this->db_host, $this->db_user, $this->db_pass, $this->db_name);

      if ($this->db->open()) {
        return true;
      } else {
        return false;
      }
  }

  /**
  * Write database settings to file
  * @return boolean
  */
  function write_settings() {
    // Store database settings to file
    if ($databasefile = @fopen("../includes/inc_db_settings.php", "w")) {
      $text = "<?php\n" .
      "// WebPA database settings created by the installer\n" .
      "// Edit as necessary\n" .
      "define('APP__DB_DRIVER', '" . $this->db_driver . "');\n" .
      "define('APP__DB_HOST', '" . $this->db_host . "');\n" .
      "define('APP__DB_USERNAME', '" . $this->db_user . "');\n" .
      "define('APP__DB_PASSWORD', '" . $this->db_pass . "');\n" .
      "define('APP__DB_DATABASE', '" . $this->db_name . "');\n" .
      "define('APP__DB_TABLE_PREFIX', '" . $this->db_prefix . "');\n" .
      "?>";

      if (@fwrite($databasefile, $text)) {
        @fclose($databasefile);
        return true;
      } else {
        @fclose($databasefile);
        return false;
      }
    } else {
      return false;
    }
  }

  // Prepared Statement
  function prepared_stmt ($sql, $vars) {
    if ($this->db->execute($sql, $vars)) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Create admin account
  * @param string $forename
  * @param string $lastname
  * @param string $email
  * @param string $username
  * @param string $password
  * @return boolean
  */
  function create_admin($forename, $lastname, $email, $username, $password) {
    // Var array
    $vars = array
    (
      ":forename" => $forename,
      ":lastname" => $lastname,
      ":email" => $email,
      ":username" => $username,
      ":password" => password_hash($password, PASSWORD_DEFAULT)
    );

    // Create admin SQL
    $createadmin_sql = "INSERT INTO " . $this->$db_prefix ."user
    SET
      forename = :forename, lastname = :lastname, email = :email, username = :username, password = :password,
      admin = 1, disabled = 0;
    INSERT INTO " . $this->$db_prefix . "module
    SET
      module_code = 'sample', module_title = 'Sample module';";

    if ($this->db->execute($createadmin_sql, $vars)) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Create tables
  * @return boolean
  */
  function create_tables() {
    // Create tables sql
    $createtables_sql = "CREATE TABLE " . $this->db_prefix . "assessment (
    assessment_id char(36) NOT NULL,
    assessment_name varchar(100) NOT NULL,
    module_id int(10) unsigned NOT NULL,
    collection_id char(36) NOT NULL,
    form_xml text NOT NULL,
    open_date datetime NOT NULL,
    close_date datetime NOT NULL,
    retract_date datetime NOT NULL,
    introduction text NOT NULL,
    allow_feedback tinyint(1) NOT NULL,
    assessment_type tinyint(1) NOT NULL,
    student_feedback tinyint(1) NOT NULL,
    email_opening tinyint(1) NOT NULL,
    email_closing tinyint(1) NOT NULL,
    contact_email varchar(255) NOT NULL,
    feedback_name varchar(45) NOT NULL,
    feedback_length varchar(45) NOT NULL,
    feedback_optional tinyint(4) NOT NULL,
    PRIMARY KEY (assessment_id)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "assessment_group_marks (
      assessment_id char(36) NOT NULL,
      group_mark_xml text NOT NULL,
      PRIMARY KEY (assessment_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "assessment_marking (
      assessment_id char(36) NOT NULL,
      date_created datetime NOT NULL,
      date_last_marked datetime NOT NULL,
      marking_params varchar(255) NOT NULL,
      PRIMARY KEY (assessment_id,date_created)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "collection (
      collection_id char(36) NOT NULL,
      module_id int(10) unsigned NOT NULL,
      collection_name varchar(50) NOT NULL,
      collection_created_on datetime NOT NULL,
      collection_locked_on datetime DEFAULT NULL,
      PRIMARY KEY (collection_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "form (
      form_id char(36) NOT NULL,
      form_name varchar(100) NOT NULL,
      form_type varchar(20) NOT NULL,
      form_xml text NOT NULL,
      PRIMARY KEY (form_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "form_module (
      form_id char(36) NOT NULL,
      module_id int(10) unsigned NOT NULL,
      PRIMARY KEY (form_id,module_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "module (
      module_id int(10) unsigned NOT NULL AUTO_INCREMENT,
      source_id varchar(255) NOT NULL DEFAULT '',
      module_code varchar(255) NOT NULL,
      module_title varchar(255) NOT NULL,
      PRIMARY KEY (module_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user (
      user_id int(10) unsigned NOT NULL AUTO_INCREMENT,
      source_id varchar(255) NOT NULL DEFAULT '',
      username varchar(255) NOT NULL,
      `password` varchar(255) NOT NULL,
      id_number varchar(255) DEFAULT NULL,
      department_id varchar(255) DEFAULT NULL,
      forename varchar(255) NOT NULL,
      lastname varchar(255) NOT NULL,
      email varchar(255) DEFAULT NULL,
      admin tinyint(1) NOT NULL DEFAULT '0',
      disabled tinyint(1) NOT NULL DEFAULT '0',
      date_last_login datetime DEFAULT NULL,
      last_module_id int(10) DEFAULT NULL,
      PRIMARY KEY (user_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_group (
      group_id char(36) NOT NULL,
      collection_id char(36) NOT NULL,
      group_name varchar(50) NOT NULL,
      PRIMARY KEY (group_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_group_member (
      group_id char(36) NOT NULL,
      user_id int(10) unsigned NOT NULL,
      PRIMARY KEY (group_id,user_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_justification (
      assessment_id char(36) NOT NULL,
      group_id char(36) NOT NULL,
      user_id int(10) unsigned NOT NULL,
      marked_user_id int(10) unsigned NOT NULL,
      justification_text text NOT NULL,
      date_marked datetime NOT NULL,
      PRIMARY KEY (assessment_id,group_id,user_id,marked_user_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_mark (
      assessment_id char(36) NOT NULL,
      group_id char(36) NOT NULL,
      user_id int(10) unsigned NOT NULL,
      marked_user_id int(10) unsigned NOT NULL,
      question_id tinyint(4) NOT NULL,
      date_marked datetime NOT NULL,
      score tinyint(4) NOT NULL,
      PRIMARY KEY (assessment_id,group_id,user_id,marked_user_id,question_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_module (
      user_id int(10) unsigned NOT NULL,
      module_id int(10) unsigned NOT NULL,
      user_type char(1) NOT NULL DEFAULT 'S',
      PRIMARY KEY (user_id,module_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_reset_request (
      id tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
      `hash` varchar(32) NOT NULL,
      user_id int(10) unsigned NOT NULL,
      PRIMARY KEY (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_response (
      assessment_id char(36) NOT NULL,
      group_id char(36) NOT NULL,
      user_id int(10) unsigned NOT NULL,
      ip_address varchar(20) NOT NULL,
      comp_name varchar(50) NOT NULL,
      date_responded datetime NOT NULL,
      date_opened datetime NOT NULL,
      PRIMARY KEY (assessment_id,group_id,user_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "user_tracking (
      user_id int(10) unsigned NOT NULL,
      `datetime` datetime NOT NULL,
      ip_address varchar(15) NOT NULL,
      description varchar(255) NOT NULL,
      module_id int(11) DEFAULT NULL,
      object_id varchar(36) DEFAULT NULL,
      PRIMARY KEY (user_id,`datetime`, description)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE " . $this->db_prefix . "setting (
      setting_id int(10) unsigned NOT NULL AUTO_INCREMENT,
      setting_internalid varchar(255) NOT NULL,
      setting_name varchar(255) NOT NULL,
      setting_description varchar(255) NOT NULL,
      setting_value varchar(255) NOT NULL,
      setting_defaultvalue varchar(255) NOT NULL,
      setting_lastchangedby int(10) unsigned DEFAULT NULL,
      PRIMARY KEY (setting_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


    --
    -- Add unique constraints
    --

    ALTER TABLE " . $this->db_prefix . "user add CONSTRAINT " . $this->db_prefix . "user_UC1 UNIQUE (
      source_id, username);

    ALTER TABLE " . $this->db_prefix . "module add CONSTRAINT " . $this->db_prefix . "module_UC1 UNIQUE (
      source_id, module_code);

    --
    -- Add referential integrity constraints
    --

    ALTER TABLE " . $this->db_prefix . "assessment
      add CONSTRAINT " . $this->db_prefix . "module_assessment_FK1 FOREIGN KEY (
        module_id)
       REFERENCES " . $this->db_prefix . "module (
        module_id);

    ALTER TABLE " . $this->db_prefix . "assessment
      add CONSTRAINT " . $this->db_prefix . "collection_assessment_FK1 FOREIGN KEY (
        collection_id)
       REFERENCES " . $this->db_prefix . "collection (
        collection_id);

    ALTER TABLE " . $this->db_prefix . "assessment_group_marks
      add CONSTRAINT " . $this->db_prefix . "assessment_assessment_group_marks_FK1 FOREIGN KEY (
        assessment_id)
       REFERENCES " . $this->db_prefix . "assessment (
        assessment_id);

    ALTER TABLE " . $this->db_prefix . "assessment_marking
      add CONSTRAINT " . $this->db_prefix . "assessment_assessment_marking_FK1 FOREIGN KEY (
        assessment_id)
       REFERENCES " . $this->db_prefix . "assessment (
        assessment_id);

    ALTER TABLE " . $this->db_prefix . "collection
      add CONSTRAINT " . $this->db_prefix . "module_collection_FK1 FOREIGN KEY (
        module_id)
       REFERENCES " . $this->db_prefix . "module (
        module_id);

    ALTER TABLE " . $this->db_prefix . "form_module
      add CONSTRAINT " . $this->db_prefix . "form_form_module_FK1 FOREIGN KEY (
        form_id)
       REFERENCES " . $this->db_prefix . "form (
        form_id);

    ALTER TABLE " . $this->db_prefix . "form_module
      add CONSTRAINT " . $this->db_prefix . "module_form_module_FK1 FOREIGN KEY (
        module_id)
       REFERENCES " . $this->db_prefix . "module(
        module_id);

    ALTER TABLE " . $this->db_prefix . "user_group
      add CONSTRAINT " . $this->db_prefix . "collection_user_group_FK1 FOREIGN KEY (
        collection_id)
       REFERENCES " . $this->db_prefix . "collection (
        collection_id);

    ALTER TABLE " . $this->db_prefix . "user_group_member
      add CONSTRAINT " . $this->db_prefix . "user_group_user_group_member_FK1 FOREIGN KEY (
        group_id)
       REFERENCES " . $this->db_prefix . "user_group (
        group_id);

    ALTER TABLE " . $this->db_prefix . "user_group_member
      add CONSTRAINT " . $this->db_prefix . "user_user_group_member_FK1 FOREIGN KEY (
        user_id)
       REFERENCES " . $this->db_prefix . "user (
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_justification
      add CONSTRAINT " . $this->db_prefix . "user_response_user_justification_FK1 FOREIGN KEY (
        assessment_id,
        group_id,
        user_id)
       REFERENCES " . $this->db_prefix . "user_response (
        assessment_id,
        group_id,
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_justification
      add CONSTRAINT " . $this->db_prefix . "user_user_justification_FK1 FOREIGN KEY (
        marked_user_id)
       REFERENCES " . $this->db_prefix . "user (
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_mark
      add CONSTRAINT " . $this->db_prefix . "user_response_user_mark_FK1 FOREIGN KEY (
        assessment_id,
        group_id,
        user_id)
       REFERENCES " . $this->db_prefix . "user_response (
        assessment_id,
        group_id,
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_module
      add CONSTRAINT " . $this->db_prefix . "module_user_module_FK1 FOREIGN KEY (
        module_id)
       REFERENCES " . $this->db_prefix . "module (
        module_id);

    ALTER TABLE " . $this->db_prefix . "user_module
      add CONSTRAINT " . $this->db_prefix . "user_user_module_FK1 FOREIGN KEY (
        user_id)
       REFERENCES " . $this->db_prefix . "user (
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_reset_request
      add CONSTRAINT " . $this->db_prefix . "user_user_reset_request_FK1 FOREIGN KEY (
        user_id)
       REFERENCES " . $this->db_prefix . "user (
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_response
      add CONSTRAINT " . $this->db_prefix . "assessment_user_response_FK1 FOREIGN KEY (
        assessment_id)
       REFERENCES " . $this->db_prefix . "assessment (
        assessment_id);

    ALTER TABLE " . $this->db_prefix . "user_response
      add CONSTRAINT " . $this->db_prefix . "user_group_member_user_response_FK1 FOREIGN KEY (
        group_id,
        user_id)
       REFERENCES " . $this->db_prefix . "user_group_member (
        group_id,
        user_id);

    ALTER TABLE " . $this->db_prefix . "user_tracking
      add CONSTRAINT " . $this->db_prefix . "user_user_tracking_FK1 FOREIGN KEY (
        user_id)
       REFERENCES " . $this->db_prefix . "user (
        user_id);

    ALTER TABLE " . $this->db_prefix . "setting
      add CONSTRAINT " . $this->db_prefix . "user_setting_FK1 FOREIGN KEY (
          setting_lastchangedby)
        REFERENCES " . $this->db_prefix . "user (
          user_id);
        )";

    // Create tables
    if ($this->db->execute($createtables_sql)) {
      return true;
    } else {
      return false;
    }
  }
}
?>
